User Recording module lets you record what your website users are doing on the
website in form of mousemovement, clicking, typing and scrolling. 

It's a simple version of functionality similar to services like mouseflow.com
or Crazy Egg.

It's meant to be used to get a knowledge about how your visitor percieves
your website. So what it does is track your visitors mouse movements, clicks,
typing and scrolling. With that data you can playback any session of any
visitor.

INSTALLATION:
User Recording relies on the GeoIP module for country recognition. This must be 
downloaded and enabled separately for this to work.

1. Unpack the User Recording folder and contents in the appropriate modules
directory of your Drupal installation.  This is probably
  sites/all/modules/
2. Enable the User Recording in the modules administration area.
3. If you're not using Drupal's default administrative account, make
sure "Setup User Recording" is enabled through permission settings.
4. Visit the setup page for User Recording on 
  admin/config/system/user_recording
5. To playback sessions visit the User Recording list on
  admin/reports/user_recording
