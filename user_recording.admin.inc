<?php

/**
 * @file
 * Page for controlling the user administration area.
 */

/**
 * Administration area for User Recording.
 */
function user_recording_admin() {
  module_load_include('inc', 'user_recording', 'user_recording.languages');
  if (module_exists('geoip')) {
    module_load_include('inc', 'geoip', 'geoip.values');
  }

  $form = array();

  $form['visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility'),
    '#description' => t('These settings modifies when the tracking should be loaded and when it should be run.'),
  );

  $form['visibility']['user_recording_path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Only records specific pages'),
  );

  $form['visibility']['user_recording_path']['user_recording_show'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('user_recording_show', '0'),
    '#options' => array(
      '0' => t('All pages except those listed'),
      '1' => t('Only the listed pages'),
      '2' => t('Pages on which this PHP code returns TRUE (experts only, requires PHP Filter module enabled and user access to enable)'),
    ),
  );

  if (!module_exists('php') || !user_access('use PHP for settings')) {
    $form['visibility']['user_recording_path']['user_recording_show']['2'] = array('#disabled' => TRUE);
  }

  $form['visibility']['user_recording_path']['user_recording_paths'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('user_recording_paths', 'admin/*'),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. <front> is the front page. If the PHP option is chosen, enter PHP code between <?php ?>. Note that executing incorrect PHP code can break your Drupal site."),
    '#required' => FALSE,
  );

  $form['visibility']['user_roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Only record specific user roles'),
  );

  $form['visibility']['user_roles']['user_recording_user_roles'] = array(
    '#type' => 'checkboxes',
    '#multiple' => TRUE,
    '#default_value' => variable_get('user_recording_user_roles', array('1')),
    '#options' => user_roles(),
  );

  $form['visibility']['user_recording_sizes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Only record specific screen sizes'),
  );

  $form['visibility']['user_recording_sizes']['user_recording_size_from_x'] = array(
    '#type' => 'textfield',
    '#field_prefix' => t('Min Width:'),
    '#size' => '2',
    '#default_value' => variable_get('user_recording_size_from_x', '0'),
    '#field_suffix' => t('px'),
  );

  $form['visibility']['user_recording_sizes']['user_recording_size_from_y'] = array(
    '#type' => 'textfield',
    '#field_prefix' => t('Min Height:'),
    '#size' => '2',
    '#default_value' => variable_get('user_recording_size_from_y', '0'),
    '#field_suffix' => t('px'),
  );

  $form['visibility']['user_recording_sizes']['user_recording_size_to_x'] = array(
    '#type' => 'textfield',
    '#field_prefix' => t('Max Width:'),
    '#size' => '2',
    '#default_value' => variable_get('user_recording_size_to_x', '0'),
    '#field_suffix' => t('px'),
  );

  $form['visibility']['user_recording_sizes']['user_recording_size_to_y'] = array(
    '#type' => 'textfield',
    '#field_prefix' => t('Max Height:'),
    '#size' => '2',
    '#description' => t('Set to zero for all sizes'),
    '#default_value' => variable_get('user_recording_size_to_y', '0'),
    '#field_suffix' => t('px'),
  );

  $form['visibility']['languages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Languages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Click the languages to save for. If no languages are selected it saves for all'),
  );

  $form['visibility']['languages']['user_recording_languages'] = array(
    '#type' => 'checkboxes',
    '#multiple' => TRUE,
    '#default_value' => variable_get('user_recording_languages', array('')),
    '#options' => _user_recording_get_languages(),
  );

  $form['savability'] = array(
    '#type' => 'fieldset',
    '#title' => t('Savability'),
    '#description' => t('These settings modifies where the tracking should be saved. Note that the tracking has to be running to be saved. So even if something is not saved the tracking might still run.'),
  );

  $form['savability']['countries'] = array(
    '#type' => 'fieldset',
    '#title' => t('Countries'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Click the countries to save for. If no countries are selected it saves for all'),
  );

  if (module_exists('geo_ip')) {
    $form['savability']['countries']['user_recording_countries'] = array(
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#default_value' => variable_get('user_recording_countries', array('')),
      '#options' => _geoip_country_values(),
    );
  }
  else {
    $form['savability']['countries']['user_recording_countries'] = array(
      '#markup' => t('You must install the !geoip module for this to work', array('!geoip' => l(t('GeoIP API'), 'https://www.drupal.org/project/geoip'))),
    );
  }

  $form['timing'] = array(
    '#type' => 'fieldset',
    '#title' => t('Timing'),
    '#description' => t('These settings modifies how often the recording is run and how often it is saved during a session.'),
  );

  $form['timing']['user_recording_save_wait'] = array(
    '#type' => 'textfield',
    '#title' => t('Save period'),
    '#description' => t('How often the session should run before saving a session. Please do not set to under 5 seconds.'),
    '#size' => '2',
    '#default_value' => variable_get('user_recording_save_wait', '5'),
    '#field_suffix' => t('sec'),
  );

  $form['timing']['user_recording_save_ratio'] = array(
    '#type' => 'textfield',
    '#title' => t('Save ratio'),
    '#description' => t('On a high traffic site it might not be possible from scalability point of view to save every session. Give the ratio of saved sessions to visitors that you want.'),
    '#size' => '3',
    '#default_value' => variable_get('user_recording_save_ratio', '1'),
    '#field_prefix' => '1/',
    '#field_suffix' => t('sessions should be saved.'),
  );

  $form['timing']['user_recording_save_max'] = array(
    '#type' => 'textfield',
    '#title' => t('Max period'),
    '#description' => t('The max amount of session length on one page to save. Note that very long running session can add up to very large amount of data in the database. 2 minutes are recommended if you do not need more.'),
    '#size' => '2',
    '#default_value' => variable_get('user_recording_save_max', '120'),
    '#field_suffix' => t('sec'),
  );

  $form['timing']['user_recording_save_cookie_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie time'),
    '#description' => t('The amount of days the cookie should be saved on a visitors data to keep track of the visitor.'),
    '#size' => '2',
    '#default_value' => variable_get('user_recording_save_cookie_time', '30'),
    '#field_suffix' => t('days'),
  );

  $form['privacy'] = array(
    '#type' => 'fieldset',
    '#title' => t('Privacy Settings'),
  );

  $form['privacy']['user_recording_save_passwords'] = array(
    '#type' => 'checkbox',
    '#title' => t('Save Text from Password Fields'),
    '#description' => t('When a user writes something into a password field, store this. This has serious security concerns when enabled since the users password is stored in free text in the database. Please do not enable this unless really necessary.'),
    '#default_value' => variable_get('user_recording_save_passwords', FALSE),
  );

  $form['privacy']['user_recording_save_ip'] = array(
    '#type' => 'checkbox',
    '#title' => t("Save IP's"),
    '#description' => t('Save session IP. Only use this if  you know it to be legal on your local market. This is for instance illegal in Germany. The user will be given an randomized hash instead if this is turned off. This software is not meant to be used to spy on individuals but rather help you with your conversion rate.'),
    '#default_value' => variable_get('user_recording_save_ip', FALSE),
  );

  $form['privacy']['user_recording_save_users'] = array(
    '#type' => 'checkbox',
    '#title' => t('Save Logged in Users'),
    '#description' => t('Save the Logged in Users id. Read above - only use this on pages where the user is very aware of being spied on - for instance for paid group usability testing.'),
    '#default_value' => variable_get('user_recording_save_users', FALSE),
  );

  return system_settings_form($form);
}

/**
 * We use validation hook to remove unnecessary data.
 */
function user_recording_admin_validate($form, &$form_state) {
  if (!module_exists('php') || !user_access('use PHP for settings')) {
    if ($form_state['values']['user_recording_show'] == 2) {
      form_set_error('user_recording_show', t('You do not have the right to enable this'));
    }
  }

  foreach ($form_state['values']['user_recording_languages'] as $key => $lang) {
    if (!$lang) {
      unset($form_state['values']['user_recording_languages'][$key]);
    }
  }

  if (module_exists('geoip')) {
    foreach ($form_state['values']['user_recording_countries'] as $key => $lang) {
      if (!$lang) {
        unset($form_state['values']['user_recording_countries'][$key]);
      }
    }
  }
}
