<?php

/**
 * @file
 * Page for outputting the playback data.
 */

/**
 * JSON output of a recording used for playback.
 */
function user_recording_get() {
  $params = drupal_get_query_parameters();
  if (isset($params['id'])) {
    $result = db_select('user_recording', 'p')->fields('p')->condition('id', $params['id'], '=')->execute()->fetchAssoc();

    // Data is sorted on time before outputted.
    $timers = array('mousedata', 'clickdata', 'scrolldata', 'keypressdata');
    foreach ($timers as $timer) {
      $type = drupal_substr($timer, 0, 1);
      foreach (json_decode($result[$timer]) as $time => $data) {
        $return['outputdata'] = explode('|', check_plain($data));
        $return['type'] = $type;
        $output['data'][round($time / 10, 0) * 10][] = $return;
      }
    }

    $output['session_time'] = $result['session_time'];

    ksort($output['data']);

    $output['url'] = $result['url'];
  }

  drupal_json_output($output);
}
