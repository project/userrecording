/**
 * @file
 * Javascript for recording the user.
 */

(function ($) {
  "use strict";

  Drupal.user_recording = {};

  Drupal.behaviors.userRecorder = {
    attach: function (context, settings) {
      var urTrackId = $.cookie('ur_track_id');
      var urTempStartTime = new Date().getTime();
      var urMouseMove = {};
      var urScroll = {};
      var urClicks = {};
      var urKeypress = {};
      var urPageId = Drupal.user_recording.ur_make_id();
      var urUnloaded = false;
      var urBrowserLanguage = window.navigator.language.toLowerCase();

      // Check if  the language matches.
      if (Drupal.settings.user_recording.save_languages.length == 0 ||
         Drupal.settings.user_recording.save_languages[0] == "" ||
         Drupal.settings.user_recording.save_languages[urBrowserLanguage]) {

        // Check if  display size is correct.
        if ((Drupal.settings.user_recording.save_to_x == 0 || Drupal.settings.user_recording.save_to_y == 0) ||
           ((Drupal.settings.user_recording.save_to_x >= screen.width && Drupal.settings.user_recording.save_from_x <= screen.width) &&
           (Drupal.settings.user_recording.save_to_y >= screen.height && Drupal.settings.user_recording.save_from_y <= screen.height))) {

          // Do the randomize or if  a recording is set.
          if ((urTrackId != "0" || Math.floor((Math.random() * Drupal.settings.user_recording.save_ratio) + 1) == 1)) {
            var lastMovement = 0;
            var counter = 0;
            var interval = (Drupal.settings.user_recording.save_wait * 1000);

            if (urTrackId == null) {
              urTrackId = Drupal.user_recording.ur_make_id();

              $.cookie('ur_track_id', urTrackId, { expires: Drupal.settings.user_recording.user_recording_save_cookie_time });
            }

            $('a').bind("click", function() {
              urClicks[(new Date().getTime() - urTempStartTime)] = event.clientX + "|" + event.clientY + "|" + event.ctrlKey + "|" + event.altKey + "|" + event.shiftKey;
              var object = $._data(this, 'events');
              if (object.click[1] != null) {
                return false;
              }
              else {
                return Drupal.user_recording.ur_send_data(false, urUnloaded, urTempStartTime, urMouseMove, urScroll, urClicks, urKeypress, urTrackId, urPageId);
              }
            });

            $('form').submit(function(event) {
              return Drupal.user_recording.ur_send_data(false, urUnloaded, urTempStartTime, urMouseMove, urScroll, urClicks, urKeypress, urTrackId, urPageId);
            });

            $(window).bind("keypress", function(event) {
              if ($(':focus').attr('type') != 'password' || Drupal.settings.user_recording.save_passwords == true) {
                urKeypress[(new Date().getTime() - urTempStartTime)] = event.charCode;
              }
            });

            $(window).bind("mousemove", function(event) {
              var changes = new Date().getTime() - urTempStartTime;
              var tenChanges = Math.round(changes / 1000);
              if (tenChanges != lastMovement) {
                tenChanges = lastMovement;
                urMouseMove[changes] = event.pageX + "|" + event.pageY;
              }
            });

            $(window).bind("scroll", function(event) {
              urScroll[(new Date().getTime() - urTempStartTime)] = $(window).scrollTop();
            });

            $(window).bind("click", function(event) {
              urClicks[(new Date().getTime() - urTempStartTime)] = event.clientX + "|" + event.clientY + "|" + event.ctrlKey + "|" + event.altKey + "|" + event.shiftKey;
            });

            var myInterval = window.setInterval(function(){
              Drupal.user_recording.ur_send_data(true, urUnloaded, urTempStartTime, urMouseMove, urScroll, urClicks, urKeypress, urTrackId, urPageId);
              counter = counter + interval;

              if (counter > (Drupal.settings.user_recording.save_max_value * 1000)) {
                clearInterval(myInterval);
                urUnloaded = true;
              }
            }, interval);
          }
          else {
            $.cookie('ur_track_id', "0", { expires: Drupal.settings.user_recording.user_recording_save_cookie_time });
          }
        }
      }
    }
  };

  Drupal.user_recording.ur_send_data = function(asyncSend, urUnloaded, urTempStartTime, urMouseMove, urScroll, urClicks, urKeypress, urTrackId, urPageId) {
    if (!urUnloaded) {
      var urEndTime = new Date().getTime();
      var urSessiontime = urEndTime - urTempStartTime;
      var urNavigator = window.navigator.userAgent;
      var urWindowsize = $(window).width() + "|" + $(window).height();
      var urScreensize = screen.width + "|" + screen.height;
      var urStarttime = Math.round(urTempStartTime / 1000);
      var urReferrer = document.referrer;
      var urHref = window.location.href;
      var urBrowserLanguage = window.navigator.language.toLowerCase();

      $.ajax({
        type: 'POST',
        url: Drupal.settings.user_recording.save_url,
        data: {
          "ur_sessiontime":urSessiontime,
          "ur_navigator":urNavigator,
          "ur_windowsize":urWindowsize,
          "ur_screensize":urScreensize,
          "ur_browserlanguage":urBrowserLanguage,
          "ur_starttime":urStarttime,
          "ur_referrer":urReferrer,
          "ur_mousemove":JSON.stringify(urMouseMove),
          "ur_scroll":JSON.stringify(urScroll),
          "ur_clicks":JSON.stringify(urClicks),
          "ur_keypress":JSON.stringify(urKeypress),
          "ur_track_id":urTrackId,
          "ur_href":urHref,
          "ur_page_id":urPageId
        },
        success: function(result) {
          return true;
        },
        async:asyncSend
      });
    }
    else {
      return true;
    }
  }

  Drupal.user_recording.ur_make_id = function() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 16; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }
})(jQuery);
