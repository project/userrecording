<?php

/**
 * @file
 * Page for controlling the user administration listing.
 */

/**
 * Admin site for checking results.
 */
function user_recording_list() {
  $rows = array();
  $output = '';

  // If no session has been chosen.
  if (!arg(3)) {
    $headers = array(
      t('Scrambled User ID'),
      t('IP'),
      t('User'),
      t('Visited Pages'),
      t('Session Time'),
      t('Language'),
      t('Country'),
      t('First session'),
      t('Browser'),
      t('Screen Size'),
    );

    $form = drupal_get_form('user_recording_searchform');
    $output .= drupal_render($form);

    $query = db_select('user_recording', 'u')->fields('u', array(
        'session_time',
        'ip',
        'uid',
        'user_id',
        'browser_language',
        'country',
        'navigator',
        'start_time',
        'browser_size',
      ))->groupBy('user_id')->orderBy('start_time', 'DESC')->extend('PagerDefault')->limit(10);

    if (isset($_GET['starturl'])) {
      $query->condition('url', $_GET['starturl'], '=');
    }

    $query->addExpression('SUM(session_time)', 's_time');
    $query->addExpression('COUNT(id)', 'pages');

    $result = $query->execute()->fetchAll();

    foreach ($result as $row) {
      $rows[] = array(
        l($row->user_id, 'admin/reports/user_recording/' . $row->user_id),
        check_plain($row->ip),
        $row->uid ? theme('username', array('account' => user_load($row->uid))) : '',
        check_plain($row->pages),
        check_plain($row->s_time) / 1000 . 's',
        check_plain($row->browser_language),
        check_plain($row->country),
        date('Y-m-d H:i:s', $row->start_time),
        check_plain($row->navigator),
        str_replace('|', 'x', check_plain($row->browser_size)),
      );
    }
  }
  // If a session has been chosen.
  else {
    $hash = arg(3);
    $previous = 0;
    $old_url = '';

    drupal_set_title(t('All pageloads by !hash', array('!hash' => $hash)));
    $headers = array(
      t('ID'),
      t('URL'),
      t('Time'),
      t('Session Time'),
      t('Replay'),
    );

    $result = db_select('user_recording', 'u')->fields('u')->condition('user_id', $hash, '=')->orderBy('start_time', 'DESC')->execute()->fetchAll();

    foreach ($result as $row) {
      if (!$row->referrer || (($row->start_time - $previous) > 6000) || ($row->referrer != $old_url)) {
        $rows[] = array(array(
            'style' => 'background: #888;',
            'data' => t('New Session'),
            'colspan' => '6',
          ),
        );
      }

      $old_url = $row->url;
      $previous = $row->start_time;
      $size = explode('|', $row->window_size);

      if (strstr($row->url, '?')) {
        $play = '<div style="cursor:pointer" onclick="window.open(\'' . check_plain($row->url) . '&user_record_id=' . $row->id . '\', \'\', \'width=' . check_plain($size[0]) . ', height=' . check_plain($size[1]) . '\');">' . t('Play page') . '</div>';
      }
      else {
        $play = '<div style="cursor:pointer" onclick="window.open(\'' . check_plain($row->url) . '?user_record_id=' . $row->id . '\', \'\', \'width=' . check_plain($size[0]) . ', height=' . check_plain($size[1]) . '\');">' . t('Play page') . '</div>';
      }
      $rows[] = array(
        'data' => array(
          check_plain($row->id),
          check_plain($row->url),
          date('Y-m-d H:i:s', $row->start_time),
          check_plain($row->session_time) / 1000 . 's',
          $play,
        ),
      );
    }
  }

  $output .= theme('pager');
  $output .= theme('table', array('header' => $headers, 'rows' => $rows));
  $output .= theme('pager');

  return $output;
}

/**
 * The search form.
 */
function user_recording_searchform() {
  $form['starturl'] = array(
    '#title' => t('Filter on Start URL of the user'),
    '#type' => 'textfield',
    '#attributes' => array('placeholder' => t('for example http://www.example.com/test?test=test')),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#attributes' => array('class' => array('btn-success')),
    '#value' => t('Filter'),
  );

  return $form;
}

/**
 * The search forms submit hook.
 */
function user_recording_searchform_submit($form, &$form_state) {
  $values['starturl'] = $form_state['values']['starturl'];
  $form_state['redirect'] = array('admin/reports/user_recording', array('query' => $values));
}
