<?php

/**
 * @file
 * Page for saving tracking data.
 */

/**
 * Page that the javascript posts the data to.
 */
function user_recording_track() {
  $params = drupal_get_query_parameters($_REQUEST);
  $var = array(
    ':user_id' => isset($params['ur_track_id']) ? $params['ur_track_id'] : '',
    ':navigator' => isset($params['ur_navigator']) ? $params['ur_navigator'] : '',
    ':start_time' => isset($params['ur_starttime']) ? $params['ur_starttime'] : '',
    ':session_time' => isset($params['ur_sessiontime']) ? $params['ur_sessiontime'] : '',
    ':window_size' => isset($params['ur_windowsize']) ? $params['ur_windowsize'] : '',
    ':browser_size' => isset($params['ur_screensize']) ? $params['ur_screensize'] : '',
    ':browser_language' => isset($params['ur_browserlanguage']) ? $params['ur_browserlanguage'] : '',
    ':clickdata' => isset($params['ur_clicks']) ? $params['ur_clicks'] : '',
    ':mousedata' => isset($params['ur_mousemove']) ? $params['ur_mousemove'] : '',
    ':scrolldata' => isset($params['ur_scroll']) ? $params['ur_scroll'] : '',
    ':keypressdata' => isset($params['ur_keypress']) ? $_REQUEST['ur_keypress'] : '',
    ':next_link' => isset($params['ur_nextlink']) ? $params['ur_nextlink'] : '',
    ':referrer' => isset($params['ur_referrer']) ? $params['ur_referrer'] : '',
    ':url' => isset($params['ur_href']) ? $params['ur_href'] : '',
    ':page_id' => isset($params['ur_page_id']) ? $params['ur_page_id'] : '',
    ':country' => module_exists('geoip') ? geoip_country_code() : '',
  );

  $countries = variable_get('user_recording_countries', array(''));

  if (count($countries) > 0 && $countries[0] != '' && !isset($countries[$var[':country']])) {
    drupal_json_output(array('status' => 'not_saved'));
    drupal_exit();
  }

  // If we should save the ips.
  $var[':ip'] = '';
  if (variable_get('user_recording_save_ip', FALSE)) {
    $var[':ip'] = ip_address();
  }

  // If we should save the user uid.
  $var[':uid'] = 0;
  if (variable_get('user_recording_save_users', FALSE)) {
    global $user;
    $var[':uid'] = $user->uid;
  }

  if ($var[':user_id']) {
    db_merge('user_recording')
      ->key(array(
        'user_id' => $var[':user_id'],
        'page_id' => $var[':page_id'],
      ))
      ->fields(array(
        'navigator' => $var[':navigator'],
        'start_time' => $var[':start_time'],
        'session_time' => $var[':session_time'],
        'window_size' => $var[':window_size'],
        'browser_size' => $var[':browser_size'],
        'browser_language' => $var[':browser_language'],
        'clickdata' => $var[':clickdata'],
        'mousedata' => $var[':mousedata'],
        'keypressdata' => $var[':keypressdata'],
        'url' => $var[':url'],
        'scrolldata' => $var[':scrolldata'],
        'next_link' => $var[':next_link'],
        'referrer' => $var[':referrer'],
        'country' => $var[':country'],
        'ip' => $var[':ip'],
        'uid' => $var[':uid'],
      ))
      ->execute();

    drupal_json_output(array('status' => 'ok'));
  }
  else {
    drupal_json_output(array('status' => 'error'));
  }
}
