/**
 * @file
 * Javascript for playing the user session.
 */

(function ($) {
  "use strict";
  Drupal.user_recording_player = {};

  Drupal.behaviors.userRecorder = {
    attach: function (context, settings) {
      var id = Drupal.user_recording_player.ur_get_param('user_record_id');
      var time = 0;
      var lastScroll = 0;
      var typing = false;
      var i;
      $('body').prepend('<div id="po-tr-mousepointer"></div>');
      $('body').prepend('<div id="po-tr-character"></div>');
      var url = '';
      if (Drupal.settings.user_recording.get_url.indexOf('\?') != -1) {
        url = Drupal.settings.user_recording.get_url + "&id=";
      }
      else {
        url = Drupal.settings.user_recording.get_url + "?id=";
      }

      $.getJSON(url + id, function(data) {
        var myInterval = window.setInterval(function(){
          if (data.data[time] != null) {
            for (i = 0; i < data.data[time].length; i++) {
              switch (data.data[time][i].type) {
                case "m":
                  typing = false;
                  $('#po-tr-mousepointer').css('left', data.data[time][i].outputdata[0] + 'px').css('top', data.data[time][i].outputdata[1] + 'px');
                  break;

                case "s":
                  typing = false;
                  console.log(data.data[time][i].outputdata[0]);
                  lastScroll = parseInt(data.data[time][i].outputdata[0]);
                  $(document).scrollTop(lastScroll);
                  break;

                case "c":
                  typing = false;
                  $('body').prepend('<div id="po-tr-click" style="left: ' + (parseInt(data.data[time][i].outputdata[0]) - 32) + 'px; top: ' + (lastScroll + parseInt(data.data[time][i].outputdata[1] - 32)) + 'px;"></div>');
                  break;

                case "k":
                  if (!typing) {
                    $('#po-tr-character').append("<br>");
                  }
                  $('#po-tr-character').append(String.fromCharCode(data.data[time][i].outputdata[0]));
                  typing = true;
                  break;

              }
            }
          }
          time = time + 10;
          if (time > parseInt(data.session_time)) {
            $('body').prepend('<div id="po-tr-stopped">' + Drupal.t('Stopped') + '</div>');
            clearInterval(myInterval);
          }
        }, 10);
      });
    }
  };

  Drupal.user_recording_player.ur_get_param = function(paramName) {
    var re = new RegExp('[?&]' + paramName + '=([^&]*)');
    var match = re.exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
  }

})(jQuery);
